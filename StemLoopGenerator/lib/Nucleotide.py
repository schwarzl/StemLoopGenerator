__author__ = 'Tom'
__VERBOSE__ = False

class Nucleotide:
    shortname = None
    states = ["A", "C", "G", "U"]
    
    def __init__(self):
        self.next = None
    
    def add_next(self, next):
        if self.next is None:
            self.next = next
        else:
            self.next.add_next(next)
        return self
    
    def evaluate(self, max_gap_count):
        pass
    
    def get_states(self, matchlist, match_index, gap_count, max_gap_count):
        return ((self.states, match_index, gap_count))
    
    # hook for 'Match' nucleotides to report state
    def report_state(self, state, matchlist):
        return matchlist
    
    def length(self):
        if self.next is not None:
            return 1 + self.next.length()
        else:
            return 1
    
    # without gaps
    def min_length(self):
        if self.next is not None:
            return 1 + self.next.min_length()
        else:
            return 1
        
    
    # checks if sequences is duplicated (same sequence, same backbone)
    # returns True, duplication_counter + 1, sequence_lookup  if is duplicated. 
    # returns False, duplication_counter, sequence_lookup if it is not duplicated, and sequence was added
    #    to sequence lookup
    def checkDuplication(self, duplication_counter, sequence_lookup, sequence, backbone):
        key = sequence + backbone 
                    
        # optimization is on and the new sequence exists in dict
        if sequence_lookup is not None:
            
            # if sequences does exist in dict, do not follow up and increment loopcounter
            if key in sequence_lookup:
                follow = False
                
                duplication_counter += 1
                
                #print ("#%s %s" % (sequence, backbone))# , str(matchlist)))
                
                return(True, duplication_counter, sequence_lookup)
                
            # sequence does not exist in dict, follow up and enter key
            else:
                sequence_lookup.add(key)
                
        return(False, duplication_counter, sequence_lookup)
    
    
    # This function finds all possible sequences and limits them to GC count
    def do_evaluate(self, sequence, gap_count, max_gap_count, sequences, matchlist, match_index, step_count, 
                    total_length, gc_counts, min_gc_content, sequence_lookup, backbone):
        
        # determines possible nucleotides (states) for the certain position
        (states, match_index, gap_count) = self.get_states(matchlist, match_index,  gap_count, max_gap_count)
        
        recursion_counter = 0 
        duplication_counter = 0
        gc_min_cutoff_counter = 0 # counter for processes stopped because of gc minimum criteria
        
        (duplicated, duplication_counter, sequence_lookup) = self.checkDuplication(duplication_counter, sequence_lookup, sequence, backbone)
        
        if not duplicated:
            # recursively explore all nucleotide variants (states)
            for state in states:
                # if C or G add to the GC count
                new_gc_counts = gc_counts
                if state == "C" or state == "G":
                    new_gc_counts += 1
                
                # create new sequence
                newsequence = sequence + state
                
                # hook for matches to report 
                newmatchlist = self.report_state(state, matchlist)
                
                # if there are still nucleotides to evaluate
                if self.next is not None:
                    # check if gc content could not be reached for time optimisation purposes
                    # current gc count plus all the possible positions (which could be gs or cs)
                    # divided by the total length, results in the maximal gc count possible
                    max_possible_gc_content = (new_gc_counts + total_length + gap_count - step_count ) / (total_length + gap_count)
                    
                    #DEBUG
                    #print("step: %s total: %s max poss: %s gcs: %s min wanted: %s seq: %s gaps: %s eval: %s" % (step_count, total_length, round(max_possible_gc_content,2) , new_gc_counts, min_gc_content, newsequence, gap_count, max_possible_gc_content > min_gc_content))
                    
                    # if the maximal gc count possible is not higher than the min gc count,
                    # then stop, otherwise continue
                    if max_possible_gc_content > min_gc_content:
                        
                        if __VERBOSE__: 
                            print("Evaluating %s, adding %s, and going to next node" % (self.shortname, state))
                        
                        new_step_count = step_count
                        if state != "":
                            new_step_count += 1
                                               
                        # only add this if it is not empty ""
                        newbackbone = backbone
                        
                        if state is not "":
                            newbackbone += self.shortname
                        
                        # increment duplication counter with all the duplications
                        # found in the downstream recursions.
                        (rec, dup, gc_min) = self.next.do_evaluate(newsequence,
                                              gap_count,
                                              max_gap_count,
                                              sequences,
                                              newmatchlist,
                                              match_index,
                                              new_step_count,
                                              total_length,
                                              new_gc_counts,
                                              min_gc_content,
                                              sequence_lookup,
                                              newbackbone)
                        
                        recursion_counter     += rec
                        duplication_counter   += dup  
                        gc_min_cutoff_counter += gc_min
                        
                    else:
                        #print("max poss: %s  min wanted: %s" % (max_possible_gc_content, min_gc_content))
                        gc_min_cutoff_counter += 1  # it is stopped
                        
                else: # if the end (a leaf) was recursively reached, add the sequence to the tree
                    if __VERBOSE__: 
                        print("Evaluating %s, adding %s, adding to final list" % (self.shortname, state))
                    
                    # check if GC constraints are met 
                    if self.validate_sequence(newsequence, new_gc_counts, min_gc_content):
                        sequences.add(newsequence) 
        
        return (recursion_counter + 1, duplication_counter, gc_min_cutoff_counter)
    
    
    def eval(self, max_gap_count, min_gc_content, optimize):
        sequences = set()
        # if runtime optimization is chosen 
        sequence_lookup = set() if optimize else None
        (recursion_counter, duplication_counter, gc_min_cutoff_counter) = \
            self.do_evaluate("", # sequence
                         0,  # gap count
                         max_gap_count, # max gap count
                         sequences, # list of sequences
                         "", # match list
                         0, # match index
                         1, # step count
                         self.min_length(), # min total length
                         0, # gc counts 
                         min_gc_content,
                         sequence_lookup, # min gc content
                         "")
        
        #print(list(sequence_lookup)[0:10])
        #print("LookupCounter: %s" % duplication_counter)
        return (sequences, recursion_counter, duplication_counter, gc_min_cutoff_counter)
    
    
    def validate_sequence(self, sequence, gc_counts, min_gc_content):
        return gc_counts / len(sequence) >= min_gc_content
    
    def __str__(self):
        return "%s" % (self.shortname)
        #return "%s(%s)" % (self.shortname, ",".join(self.states))
    
    def str_all(self):
        if self.next is not None:
            return str(self) + self.next.str_all()
        else:
            return str(self)
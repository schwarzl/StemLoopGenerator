__author__ = 'Tom'
__VERBOSE__ = False

from lib.Nucleotide import Nucleotide

class LoopOption(Nucleotide):
    shortname = "O"
    states = ["", "A", "C", "G", "U"]
    
    def __init__(self):
        super().__init__()
            
    # min length without gaps
    def min_length(self):
        if self.next is not None:
            return self.next.min_length()
        else:
            return 0 
__author__ = 'Tom'
__VERBOSE__ = False

from lib.Nucleotide import Nucleotide

# This 
class MatchMate(Nucleotide):
    states = [ "X" ]
    shortname = "N"
    
    def __init__(self):
        super().__init__()
        
    def get_states(self, matchlist, match_index, gap_count, max_gap_count):
        mate_state = matchlist[ match_index ]
        match_index += 1
        
        if __VERBOSE__: 
            print("Got the mate state %s" % (mate_state))
        if mate_state == "A":
            return( ([ "U" ], match_index, gap_count ) )
        elif mate_state == "U":
            return( ([ "A" ], match_index, gap_count ) )
        elif mate_state == "C":
            return( ([ "G" ], match_index, gap_count ))
        elif mate_state == "G":
            return( ( [ "C" ], match_index, gap_count))
        else:
            Exception("unknown mate state %s" % mate_state)

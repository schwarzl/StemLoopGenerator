__author__ = 'Tom'
__VERBOSE__ = False

from lib.Nucleotide import Nucleotide

# Class Loop derived from Nucleotide
class Loop(Nucleotide):
    shortname = "L"
    
    def __init__(self):
        super().__init__()
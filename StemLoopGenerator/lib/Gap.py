__author__ = 'Tom'
__VERBOSE__ = False

from lib.Nucleotide import Nucleotide

class Gap(Nucleotide):
    shortname = "g"
    states = ["", "A", "C", "G", "U"]
    
    def __init__(self):
        super().__init__()
        
    def get_states(self, matchlist, match_index, gap_count, max_gap_count):
        if gap_count >= max_gap_count:
            return(([""], match_index, gap_count ))
        else:
            return(self.states, match_index, gap_count + 1)
    
    # min length without gaps
    def min_length(self):
        if self.next is not None:
            return self.next.min_length()
        else:
            return 0 
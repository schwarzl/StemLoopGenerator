__author__ = 'Tom'
__VERBOSE__ = False

#import lib
#from Nucleotide import Nucleotide
from lib.Nucleotide import Nucleotide

# This class manages a stem loop as stem loops always start with a match
# 
class Match(Nucleotide): 
    shortname = "M"
    
    def __init__(self):
        super().__init__()
        
    # only matches have to report states
    def report_state(self, state, matchlist):
        
        if __VERBOSE__: 
            print("Reporting state %s to matchlist %s" % (state, matchlist))
    
        matchlist += state #matchlist.append(state)
        
        #print(matchlist)
        return matchlist
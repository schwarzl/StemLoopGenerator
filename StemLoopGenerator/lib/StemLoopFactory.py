__author__ = 'Tom'

import sys, time, datetime, os, psutil, humanize

from ago import human
from lib.Nucleotide import Nucleotide
from lib.Match import Match
from lib.MatchMate import MatchMate
from lib.Gap import Gap
from lib.Loop import Loop
from lib.LoopOption import LoopOption

class StemLoopFactory:
    def __init__(self):
        self.loops = list()
        self.max_gap_size = 0 
        self.max_total_gap_count = 0 
        self.min_loop_size = 4
        self.max_loop_size = 6
        self.stem_size = 6
        self.min_matches_before_first_gap = 0
        self.min_gc_content = 0
        self.stemloop = None
        self.sequences = None
        self.optimize = None
        self.recursion_count = 0
        self.duplication_count = 0
        self.gc_min_cutoff_counter = 0
        self.calculate_sequences_time = 0
        self.print_sequences_time = 0
        self.memory_use = 0
    
    def init_stemloop(self):
        first = True
        
        # MATCHES
        for i in range(0, self.stem_size):
            # start with a match
            if first:
                n = Match()
                first = False
            else:
                n = n.add_next(Match())
            
            # GAPS
            # prevent gaps in the beginning of the loops set by this
            # parameter self.min_matches_before_first_gap
            if i < self.stem_size - self.min_matches_before_first_gap:
                for i in range(0, self.max_gap_size):
                    n = n.add_next(Gap())
        
        # LOOP
        for i in range(0, self.min_loop_size):
            n = n.add_next(Loop())
        
        # OPTIONAL LOOP
        for i in range(0, self.max_loop_size - self.min_loop_size):
            n = n.add_next(LoopOption())
            
        # MATCHES
        for i in range(0, self.stem_size - 1):
            n = n.add_next(MatchMate())
            
            # GAPS
            # prevent gaps in the beginning of the loops set by this
            # parameter self.min_matches_before_first_gap
            if i >= self.min_matches_before_first_gap - 1:
                for i in range(0, self.max_gap_size):
                    n = n.add_next(Gap())
        
        # end with a match
        n.add_next(MatchMate())
        
        self.stemloop = n
    
    def check_init(self):
        if self.stemloop is None:
            self.init_stemloop()
        
    def min_length(self):
        self.check_init()
        return self.stemloop.min_length()
    
    def length(self):
        self.check_init()
        return self.stemloop.length()
    
    def backbone(self):
        self.check_init()
        return self.stemloop.str_all()
    
    def backbone_structure(self):
        s = self.backbone()
        s = s.replace(Gap.shortname, ".")
        s = s.replace(MatchMate.shortname, ")")
        s = s.replace(Match.shortname, "(")
        s = s.replace(Loop.shortname, ".")
        s = s.replace(LoopOption.shortname, ".")
        return s
        
    def summary(self, version = "unkown"):
        out =  "# STEM LOOP GENERATOR v%s\n" % version
        out += "# ========================================================== #\n"
        out += "# stem size: %s\n" % self.stem_size
        out += "# loop size: min %s, max %s\n" % (self.min_loop_size, self.max_loop_size)
        out += "# gap size: max %s\n" % self.max_gap_size
        out += "# total gap count: max %s\n" % self.max_total_gap_count
        out += "# matches before first gap: min %s\n" % self.min_matches_before_first_gap
        out += "# gc content [0-1]: min %s\n" % self.min_gc_content
        out += "# ---------------------------------------------------------- #\n"
        out += "# Backbone: \n"
        out += "# \n"
        out += "# " + self.backbone() + "\n"
        out += "# " + self.backbone_structure() + "\n"
        out += "# \n"
        out += "# M Match, N Mate, L Loop, O Loop (optional), g Gap\n"
        out += "# \n"
        out += "# sequence length: min %s, max %s, total %s\n" % (self.min_length(),
                                                                  (self.min_length() + min(self.max_gap_size, self.max_total_gap_count)),
                                                                  self.length())
        out += "# ---------------------------------------------------------- #"
        return(out)
    
    def sequences_summary(self):
        out = "# %s recursions calculated in %s \n" % (humanize.intcomma(self.recursion_count),
                                                           human(datetime.datetime.now() 
                                                                  - datetime.timedelta(seconds=self.calculate_sequences_time),
                                                                 precision = 10,
                                                                 past_tense = '{}'))
        if self.optimize:
            out += "# %s duplicated recursions were stopped using optimization\n" % self.duplication_count
        else:
            out += "# no optimization was chosen\n"
        if self.min_gc_content <= 0:
            out += "# no minimal gc content cutoff was chosen\n"
        else:
            out += "# %s recursions stopped because of minimal gc content cutoff of %s\n" % (self.gc_min_cutoff_counter, self.min_gc_content)
        out += "# %s memory was used \n" % humanize.naturalsize(self.memory_use)
        out += "# ---------------------------------------------------------- #\n"
        out +=  "# %s unique sequences were printed in %s \n" % (humanize.intcomma(self.sequences_count()), 
                                                                         human(datetime.datetime.now() 
                                                                          - datetime.timedelta(seconds=self.print_sequences_time),
                                                                         precision = 10,
                                                                         past_tense = '{}'))
        out += "# example sequences: %s\n" % self.sequences_header(10) 
        out += "# ========================================================== #" 

        return(out)
        
    def check_sequences_init(self):
        if self.sequences is None:
            self.calculate_sequences()
    
    def calculate_sequences(self):
        self.check_init()
        
        start_time = time.time()

        (self.sequences, 
         self.recursion_count, 
         self.duplication_count, 
         self.gc_min_cutoff_counter) = self.stemloop.eval(self.max_gap_size,
                                                          self.min_gc_content,
                                                          self.optimize)
        end_time = time.time()
        
        self.calculate_sequences_time = end_time - start_time

        process = psutil.Process(os.getpid())
        self.memory_use = process.memory_info().rss

    def sequences_count(self):
        self.check_sequences_init()
        return(len(self.sequences))
    
    def print_sequences(self, file):
        self.check_sequences_init()
        
        start_time = time.time()
        
        fh = sys.stdout
        
        try:
            if file is not "" and file is not None:
                fh = open(file, 'w')
            
            for sequence in self.sequences:
                fh.write("%s\n" % sequence)
            
            if file is not "" and file is not None:
                fh.close()
        except IOError as e:
            print("IOError, problems opening or writing to the file:\n" + str(e))
        
        end_time = time.time()
        
        self.print_sequences_time = end_time - start_time
    
    def sequences_header(self, i):
        return list(self.sequences)[0:i]
        
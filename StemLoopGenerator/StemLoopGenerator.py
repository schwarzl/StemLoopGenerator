# STEMLOOPGENERATOR
# by Thomas Schwarzl, schwarzl@embl.de 
# ---------------------------------------------------------------------------------
# The program builds up possible stem loop backbone structure 
# and recursively produces all possible stem loops with 
# certain properties like GC content, loop size or gaps between
# the matching pairs
#                  _
#     (O)i          | <- Loop Option
#   L      L        | RNA LOOP 
#    L    L        _| ( usually between 4 and 9 nucleotides )
#     M N           | 
#     M N           | RNA STEM
#   (G)j            | 
#     M N           | ( occur between matching nucleotides 
#     M N           |   and can be optional or 1 or many nucleotides )
#       (G)j        | 
#     M N          _| 
#
# M ... nucleotide for match M::N
# N ... nucleotide for match N::M
#       -> number of matching pairs for the stem can be controlled
# L ... nucleotide for Loop
#       -> minimal and maximal number of nucleotides for loops can be controlled
# O ...  optional nucleotide for Loop
# G ... Gap
#       -> can only occur in the stem 
#       -> are optional
#       -> can be 0 to many nucleotides long, maximum gap size can be controlled
#       -> total number of gaps can be controlled
# -> Stem length and loop length can be controlled
#
# Optimization:
#  There are cases e.g. with the backbone of MggN (Match, Gap, Gap, Match Mate)
#  where A) the first gap g and then B) the second gap g is blank, therefore resulting in:
#  A) MgN
#  B) MgN
#  which results in the same sequences.
# There ere of course cases where this would lead to exactly the same evaluation
# of the same sequences, the longer the sequences get, exponentially more
# similar calculations are done, also if the gap sizes increase this can \
# exponentially increase - e.g. MggggggggggN
# Therefore a set is passed on with the sequences to be evaluated, also
# the backbone (e.g. MggggN), where the backbone matches the passed on sequences 
# (so when a gap g is empty, then it will not be added to the backbone).
# The optimization is especially recommended when using a high number of
# gaps or optional loops. Keeping a list of unique sequences (set) at the runtime
# requires memory. Eventually this is a compromise between memory and cpu usage, 
# however if you can afford the memory, it avoids a lot of duplicated calculations,
# ergo a lot of time.
#
#
# TODO: Test if match list has to be the same as well
#
# REQUIRED PYTHON VERSION: >= 3
# REQUIRED PACKAGES: psutil
# ---------------------------------------------------------------------------------
# Default variables

# program version
__VERSION__ = 0.1

# verbose for debug purposes
__VERBOSE__ = False

# out file name (empty for writing to stdout)
OUTFILE = ""  

# config output (empty for writing to stderr)
LOGOUT = ""

# stem size
STEM_SIZE = 5

# loop size 
MIN_LOOP_SIZE = 4

# loop size 
MAX_LOOP_SIZE = 6

# maximum gap size
MAX_GAP_SIZE = 2

# maximum total gap count
MAX_TOTAL_GAP_COUNT = 2

# how many matches must be after the loops without gaps
MIN_MATCHES_BEFORE_FIRST_GAP = 2

# minimum GC content
MIN_GC_CONTENT = 0.0

# Optimize
OPTIMIZE = True


__HELP__ = '''
SYNOPSIS

    StemLoopGenerator.py <outputFile> [options]

DESCRIPTION

    
    <outputFile>          output file for all sequences
                          empty for writing to stdout
                          default: %s 
    
    [-l, --log=STRING] 
                          log out
                          empty for writing to stderr
                          default: %s
                          
    [-s, --stem-size=INT] nucleotides of stem
                          default: %s
                          
    [-m, --min-loop-size=INT]
                          min loop size
                          default: %s
                          
    [-n, --max-loop-size=INT] 
                          max loop size
                          default: %s

    [-a, --max-gap-size=INT]
                          maximum gap size
                          default: %s
                          
    [-g, --max-total-gaps=INT]
                          maximum total gap count
                          default: %s
                          
    [-b, --min-matches-before-gap=INT]
                          strong hits from every experiment in a single file
                          default: %s
    
    [-c, --min-gc-content] 
                          minimum gc content of generated sequence
                          default: %s
    
    [-o, --optimize]      
                          optimize the recursive evalution by
                          avoiding duplication, memory intensive
                          
    [-t, --test]          test run without sequence evaluation
    
    [-h, --help]          display help message

    [--verbose]           display verbose output for debugging

EXAMPLES

    python StemLoopGenerator.py > sequences.txt 2> summary.txt
    python StemLoopGenerator.py sequences.txt
    python StemLoopGenerator.py --help

AUTHOR

    Thomas Schwarzl <schwarzl@embl.de>

''' % (OUTFILE,
       LOGOUT,
       STEM_SIZE, 
       MIN_LOOP_SIZE, 
       MAX_LOOP_SIZE,
       MAX_GAP_SIZE,
       MAX_TOTAL_GAP_COUNT, 
       MIN_MATCHES_BEFORE_FIRST_GAP,
       MIN_GC_CONTENT)

import lib, sys, optparse, logging, traceback, os
from lib.StemLoopFactory import StemLoopFactory

# Logging
LOGGER = logging.getLogger()

def exceptionHandling(exception):
    global options
    if options.__VERBOSE__:
        traceback.print_exc()
    print(str(exception))
    os._exit(1)

def main():
    global options, args
    
    # set output file
    OUTFILE = args[0] if len(args) > 0 else None
    
    # initialise logger
    logFH = init_logger(options.LOGOUT)  
    
    # create stem look factory 
    f = StemLoopFactory()
    f.max_gap_size                 = options.MAX_GAP_SIZE
    f.max_total_gap_count          = options.MAX_TOTAL_GAP_COUNT
    f.min_loop_size                = options.MIN_LOOP_SIZE
    f.max_loop_size                = options.MAX_LOOP_SIZE
    f.stem_size                    = options.STEM_SIZE
    f.min_matches_before_first_gap = options.MIN_MATCHES_BEFORE_FIRST_GAP
    f.min_gc_content               = options.MIN_GC_CONTENT
    f.optimize                     = options.OPTIMIZE
    
    #logging.getLogger().setLevel(logging.INFO)
    
    # print the summary
    LOGGER.warning(f.summary(__VERSION__))
    
    # if it is not a test run, print a version
    if not options.TEST:
        f.init_stemloop()
        
        f.calculate_sequences()
        
        LOGGER.warning(f.sequences_summary()) 
        
        f.print_sequences(OUTFILE)

    # close to avoid broken pipe
    sys.stderr.close()
    

def init_logger(LOGOUT):
    # configure logger
    if options.__VERBOSE__:
        LOGGER.setLevel(logging.DEBUG)
    else:
        LOGGER.setLevel(logging.INFO)
    
    logFH = None
    if options.LOGOUT is None or options.LOGOUT is not "":
        logFH = logging.FileHandler(options.LOGOUT)
        LOGGER.addHandler(logFH)
    
    return logFH

# program can be used as standalone or as module
if __name__ == '__main__':
    try:
        # get parser and add arguments
        parser = optparse.OptionParser(usage=__HELP__,
                                       #usage=globals()['__doc__'],
                                       version="%s" % (__VERSION__))
        
        parser.add_option('-s', '--stem-size',
                          action  = 'store',
                          default = STEM_SIZE,
                          type    = "int",
                          dest    = "STEM_SIZE")
        
        parser.add_option('-m', '--min-loop-size',
                          action  = 'store',
                          default = MIN_LOOP_SIZE,
                          type    = "int",
                          dest    = 'MIN_LOOP_SIZE')

        parser.add_option('-n', '--max-loop-size',
                          action  = 'store',
                          default = MAX_LOOP_SIZE,
                          type    = "int",
                          dest    = 'MAX_LOOP_SIZE')
        
        parser.add_option('-a', '--max-gap-size',
                          action  = 'store',
                          default = MAX_GAP_SIZE,
                          type    = "int",
                          dest    = 'MAX_GAP_SIZE')
        
        parser.add_option('-g', '--max-total-gaps',
                          action  = 'store',
                          default = MAX_TOTAL_GAP_COUNT,
                          type    = "int",
                          dest    = 'MAX_TOTAL_GAP_COUNT')
        
        parser.add_option('-b', '--min-matches-before-gap',
                          action  = 'store',
                          default = MIN_MATCHES_BEFORE_FIRST_GAP,
                          type    = "int",
                          dest    = 'MIN_MATCHES_BEFORE_FIRST_GAP')

        parser.add_option('-c', '--min-gc-content',
                          action  = 'store',
                          default = MIN_GC_CONTENT,
                          type    = "float",
                          dest    = 'MIN_GC_CONTENT')

        parser.add_option('-l', '--log',
                          action  ='store',
                          default = LOGOUT,
                          type    = "string",
                          dest    = "LOGOUT")
        
        parser.add_option('-t', '--test',
                          action  = 'store_true',
                          default = False,
                          dest    = "TEST")
        
        parser.add_option('-o', '--optimize',
                          action  = 'store_true',
                          default = False,
                          dest    = "OPTIMIZE")
        
        parser.add_option('-v', '--verbose',
                          action  = 'store_true',
                          default = __VERBOSE__,
                          dest    = "__VERBOSE__")
        
        (options, args) = parser.parse_args()
        
        # check if there are all arguments
        if len(args) > 1:
            parser.error ('too many arguments')
        
        if options.MIN_LOOP_SIZE > options.MAX_LOOP_SIZE:
            raise Exception("minimal loop size cannot be bigger than maximal loop size")
        if not options.MIN_GC_CONTENT >= 0.0 and options.MIN_GC_CONTENT <= 1.0:
            raise Exception("minimal gc content has to be between 0 and 1 [0% and 100%]")
        if options.MAX_GAP_SIZE < 0:
            raise Exception("maximal gap size has to be greater or equal 0")
        if options.MAX_TOTAL_GAP_COUNT < 0:
            raise Exception("maximal total gap count has to be greater or equal 0")
        if options.MIN_MATCHES_BEFORE_FIRST_GAP < 1:
            raise Exception("minimal matches before first gap has to be greater or equal 1")
        if options.STEM_SIZE < 1:
            raise Exception("stem size has to be greater than 0")
            
        main()
        
    # Exception Handling: Interruption / Errors
    except KeyboardInterrupt as exception: # Control - C
        print("Program was stopped. (Ctrl + C / KeyboardInterrupt)")
        sys.stderr.close()
        os._exit(1)
    except SystemExit as exception:
        raise exception
    except Exception as exception:
        print("------------[ Error ]--------------")
        exceptionHandling(exception)

# TODO LOGGER 